import { Path, UseFormRegister } from 'react-hook-form';
import IFormRegister from '../register/IFormRegister.interface';

interface InputInterface {
  type: string;
  name: Path<IFormRegister>;
  placeholder: string;
  register: UseFormRegister<IFormRegister>;
  error: string;
  inputsClassName?: string;
}

export default InputInterface;
