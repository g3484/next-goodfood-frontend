interface IFormRegister {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  password: string;
  confirmPassword: string;
  address: string;
  city: string;
  zipCode: string;
}

export default IFormRegister;
