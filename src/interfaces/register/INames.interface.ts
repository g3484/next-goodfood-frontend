import IFormRegister from './IFormRegister.interface';

interface INames {
  onSubmit: (data: IFormRegister) => void;
}

export default INames;
