import { Path } from 'react-hook-form';
import IFormRegister from '../../interfaces/register/IFormRegister.interface';

export const inputRegisterNamesData = [
  {
    type: 'text',
    placeholder: 'Prénom',
    name: 'firstName' as Path<IFormRegister>,
  },
  {
    type: 'text',
    placeholder: 'Nom de famille',
    name: 'lastName' as Path<IFormRegister>,
  },
];
