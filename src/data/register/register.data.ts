import { Path } from 'react-hook-form';
import IFormRegister from '../../interfaces/register/IFormRegister.interface';

export const inputRegisterDataStep1 = [
  {
    type: 'text',
    placeholder: 'Prénom',
    name: 'firstName' as Path<IFormRegister>,
  },
  {
    type: 'text',
    placeholder: 'Nom de famille',
    name: 'lastName' as Path<IFormRegister>,
  },
];

export const inputRegisterDataStep2 = [
  {
    type: 'email',
    placeholder: 'Email',
    name: 'email' as Path<IFormRegister>,
  },
  {
    type: 'tel',
    placeholder: 'Téléphone',
    name: 'phone' as Path<IFormRegister>,
  },
  {
    type: 'password',
    placeholder: 'Mot de passe',
    name: 'password' as Path<IFormRegister>,
  },
  {
    type: 'password',
    placeholder: 'Confirmation mot de passe',
    name: 'confirmPassword' as Path<IFormRegister>,
  },
];
