import { Path } from 'react-hook-form';
import IFormRegister from '../../interfaces/register/IFormRegister.interface';

export const inputRegisterCredentialsData = [
  {
    type: 'email',
    placeholder: 'Email',
    name: 'email' as Path<IFormRegister>,
  },
  {
    type: 'tel',
    placeholder: 'Téléphone',
    name: 'phone' as Path<IFormRegister>,
  },
  {
    type: 'password',
    placeholder: 'Mot de passe',
    name: 'password' as Path<IFormRegister>,
  },
  {
    type: 'password',
    placeholder: 'Confirmation mot de passe',
    name: 'confirmPassword' as Path<IFormRegister>,
  },
];
