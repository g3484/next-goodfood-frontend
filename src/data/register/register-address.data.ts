import { Path } from 'react-hook-form';
import IFormRegister from '../../interfaces/register/IFormRegister.interface';

export const inputRegisterAddressData = [
  {
    type: 'text',
    placeholder: 'Adresse',
    name: 'address' as Path<IFormRegister>,
  },
  {
    type: 'text',
    placeholder: 'Ville',
    name: 'city' as Path<IFormRegister>,
  },
  {
    type: 'text',
    placeholder: 'Code Postal',
    name: 'zipCode' as Path<IFormRegister>,
  },
];
