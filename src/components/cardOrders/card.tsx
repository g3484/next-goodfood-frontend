import React from 'react';
import styles from './card.module.scss';

interface ICard {
  src: string;
  alt: string;
  title: string;
  price: string;
  date: string;
  status: string;
  id?: string;
  className?: string;
}

const Card = ({ src, alt, title, price, date, status, id }: ICard) => {
  return (
    <div className={styles.Card} id={id}>
      <img src={src} alt={alt} className={styles.Card__image} />
      <div className={styles.Card__description}>
        <h2>
          Commande <span>{title}</span>
        </h2>
        <p className={styles.Card__description__prix}>{price}</p>
        <p className={styles.Card__description__date}>{date}</p>
        <p className={styles.Card__description__status}>{status}</p>
      </div>
    </div>
  );
};

export default Card;
