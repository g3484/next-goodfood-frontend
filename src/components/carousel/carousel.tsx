import React from 'react';
import styles from './carousel.module.scss';

interface ICarousel {
  children: JSX.Element[] | JSX.Element;
  className?: string;
}

const Carousel = ({ children, className }: ICarousel) => {
  return <div className={`${styles.Carousel} ${className}`}>{children}</div>;
};

export default Carousel;
