import React from 'react';
import styles from './BackArrow.module.scss';
import PropTypes from 'prop-types';

const BackArrow = ({ onClick }) => {
  return (
    <div className={styles.backArrow}>
      <img
        src={`/svg/icn-back.svg`}
        onClick={onClick ? onClick : () => history.go(-1)}
      />
    </div>
  );
};

BackArrow.propTypes = {
  onClick: PropTypes.func,
};

export default BackArrow;
