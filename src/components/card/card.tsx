import React from 'react';
import styles from './card.module.scss';

interface ICard {
  src: string;
  alt: string;
  title: string;
  description: string;
  price: number;
  className?: string;
}

const Card = ({ src, alt, title, description, price, className }: ICard) => {
  return (
    <div className={`${styles.Card} ${className}`}>
      <img src={src} alt={alt} className={styles.Card__image} />
      <div className={styles.Card__content}>
        <h4>{title}</h4>
        <span className={styles.Card__content__description}>{description}</span>
        <p className={styles.Card__content__prix}>{price} €</p>
      </div>
    </div>
  );
};

export default Card;
