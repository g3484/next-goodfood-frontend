import React from 'react';
import Header from './header/header';
import Navbar from './navbar/navbar';

interface LayoutProps {
  children: React.ReactNode;
  active?: string;
  hasHeader?: boolean;
  hasNavbar?: boolean;
}

const Layout = ({
  children,
  active,
  hasHeader = true,
  hasNavbar = true,
}: LayoutProps) => {
  return (
    <main className="layout">
      {hasHeader && <Header />}
      {children}
      {hasNavbar && <Navbar active={active} />}
    </main>
  );
};

export default Layout;
