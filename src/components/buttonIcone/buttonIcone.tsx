import React from 'react';
import Link from 'next/link';

interface IButtonIcone {
  children: JSX.Element;
  className?: string;
  href?: string;
}

const ButtonIcone = ({ children, className, href }: IButtonIcone) => {
  if (href) {
    return (
      <Link href={href}>
        <a className={className}>{children}</a>
      </Link>
    );
  }
  return <button className={className}>{children}</button>;
};

export default ButtonIcone;
