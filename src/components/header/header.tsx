import React, { useEffect } from 'react';
import styles from './header.module.scss';
import useCurrentUserStore from '../../store/currentUserStore';

const header = () => {
  const { address } = useCurrentUserStore();
  useEffect(() => {
    window.addEventListener('scroll', () => {
      const header = document.getElementById('header');

      if (!header) return;

      if (window.scrollY > 50) {
        header.classList.add(styles.header__scrolled);
      } else {
        header.classList.remove(styles.header__scrolled);
      }
    });
  }, []);

  return (
    <div className={styles.header} id="header">
      <div className={styles.header__address}>
        <img src="/svg/icn-map.svg" alt="" />
        <select name="" id="">
          <option value="">{address.lane}</option>
        </select>
      </div>

      <img
        className={styles.header__filter}
        src="/svg/icn-filters.svg"
        alt=""
      />
    </div>
  );
};

export default header;
