import React from 'react';
import styles from './navbar.module.scss';
import Link from 'next/link';
import { useCartStore } from '../../store/cartStore';

interface NavbarProps {
  active: string;
}

const Navbar = ({ active }: NavbarProps) => {
  const { getAllQuantity } = useCartStore();
  return (
    <div className={styles.Navbar}>
      <Link href="/home">
        <img
          src="/svg/icn-home.svg"
          alt=""
          style={active === 'home' ? {} : { opacity: 0.5 }}
        />
      </Link>
      <Link href="/orders">
        <img
          src="/svg/icn-receipt.svg"
          alt=""
          style={active === 'orders' ? {} : { opacity: 0.5 }}
        />
      </Link>

      <img src="/svg/icn-search.svg" alt="" className={styles.Navbar__search} />

      <Link href="/cart">
        <div className={styles.Navbar__cart}>
          <img
            src="/svg/icn-cart.svg"
            alt=""
            style={active === 'cart' ? {} : { opacity: 0.5 }}
          />
          {getAllQuantity() > 0 && (
            <div className={styles.Navbar__cart__badge}>
              <span>{getAllQuantity()}</span>
            </div>
          )}
        </div>
      </Link>

      <Link href="/settings">
        <img
          src="/svg/icn-settings.svg"
          alt=""
          style={active === 'settings' ? {} : { opacity: 0.5 }}
        />
      </Link>
    </div>
  );
};

export default Navbar;
