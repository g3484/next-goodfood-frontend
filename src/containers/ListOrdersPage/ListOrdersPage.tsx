import React from 'react';
import Link from 'next/link';
import Card from '../../components/cardOrders/card';
import styles from './ListOrdersPage.module.scss';

const ListOrdersPage = ({ orders }) => {
  const formatDate = (date: string) => {
    const orderDate = new Date(date);
    const day =
      orderDate.getDate() < 10
        ? '0' + orderDate.getDate()
        : orderDate.getDate();
    const month =
      orderDate.getMonth() + 1 < 10
        ? '0' + orderDate.getMonth() + 1
        : orderDate.getMonth() + 1;
    const year = orderDate.getFullYear();

    const hours =
      orderDate.getHours() < 10
        ? '0' + orderDate.getHours()
        : orderDate.getHours();
    const minutes =
      orderDate.getMinutes() < 10
        ? '0' + orderDate.getMinutes()
        : orderDate.getMinutes();

    return `${day}/${month}/${year} à ${hours}h${minutes}`;
  };

  return (
    <div className={styles.ListOrdersPage}>
      <h1 className={styles.ListOrdersPage__titleOrders}>Vos commandes</h1>
      <div className={styles.ListOrdersPage__orders}>
        {orders
          .filter((order) => order.status !== 'cancelled')
          .map((order) => {
            let href = '';

            if (order.status === 'created')
              href = `/orders/checkout/${order.id}`;
            if (order.status === 'awaiting_delivery')
              href = `/orders/delivery/${order.id}`;
            if (order.status === 'delivery_in_progress')
              href = `/orders/delivery/${order.id}`;
            if (order.status === 'completed') href = `/orders/${order.id}`;

            return (
              <Link href={href} key={order.id}>
                <a className={styles.ListOrdersPage__orders__item}>
                  <Card
                    src={order.products[0].imageUrl}
                    alt={order.products[0].name}
                    title={`#${order.id.slice(0, 10)}`}
                    price={`${order.price} €`}
                    date={formatDate(order.dateOfOrder)}
                    status={order.status}
                  />
                </a>
              </Link>
            );
          })}
      </div>
    </div>
  );
};

export default ListOrdersPage;
