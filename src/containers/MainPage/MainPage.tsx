import React, { useState, useEffect } from 'react';
import styles from './MainPage.module.scss';
import Carousel from '../../components/carousel/carousel';
import Button from '../../components/button/button';
import Card from '../../components/card/card';
import Link from 'next/link';
import {
  getProducts,
  getProductByCatId,
  getCategories,
} from '../../services/axios/productRequest';
import Spinner from '../../components/Spinner/Spinner';

type Category = {
  id: string;
  name: string;
};

type Product = {
  id: string;
  name: string;
  imageUrl: string;
  price: number;
  description: string;
  idCategory: string;
};

const MainPage = () => {
  const [loading, setLoading] = useState(false);
  const [categories, setCategories] = useState<Category[]>([]);
  const [products, setProducts] = useState<Product[]>([]);
  const [catActive, setCatActive] = useState('Tous');

  const fetchCategories = async () => {
    const {
      data,
      status,
    }: {
      data: Category[];
      status: number;
    } = await getCategories('/api/products/category');

    if (status === 200) {
      return setCategories(data);
    }

    if (status !== 200) {
      // TODO: handle errors
      alert('error');
    }
  };

  const fetchProducts = async () => {
    setLoading(true);
    const {
      data,
      status,
    }: {
      data: Product[];
      status: number;
    } = await getProducts('/api/products');

    if (status === 200) {
      setLoading(false);
      return setProducts(data);
    }

    if (status !== 200) {
      // TODO: handle errors
      alert('error');
    }
  };

  const fetchProductsByCat = async () => {
    setLoading(true);
    const {
      data,
      status,
    }: {
      data: Product[];
      status: number;
    } = await getProductByCatId(`/api/products/categoryId/${catActive}`);

    if (status === 200) {
      setLoading(false);
      return setProducts(data);
    }

    if (status !== 200) {
      // TODO: handle errors
      alert('error');
    }
  };

  const shuffleArray = (array) => {
    let curId = array.length;
    // There remain elements to shuffle
    while (0 !== curId) {
      // Pick a remaining element
      const randId = Math.floor(Math.random() * curId);
      curId -= 1;
      // Swap it with the current element.
      const tmp = array[curId];
      array[curId] = array[randId];
      array[randId] = tmp;
    }
    return array;
  };

  const displayCategory = () => {
    return (
      <>
        {categories.map((category) => {
          return (
            <Button
              key={category.id}
              className={styles.MainPage__filters__item}
              active={
                catActive === category.id
                  ? styles.MainPage__filters__item__active
                  : ''
              }
              onClick={() => setCatActive(category.id)}
            >
              {category.name}
            </Button>
          );
        })}
      </>
    );
  };

  const displayPopularProduct = () => {
    const popular = shuffleArray(products).slice(0, 7);

    return (
      <>
        {popular.map((product) => {
          return (
            <Link href={`/home/${product.id}`} key={product.id}>
              <a style={{ textDecoration: 'none' }}>
                <div
                  key={product.id}
                  className={styles.MainPage__popular__carouselItem}
                >
                  <img
                    src={product.imageUrl}
                    alt={product.name}
                    className={styles.MainPage__popular__carouselItem__img}
                  />
                  <div
                    className={
                      styles.MainPage__popular__carouselItem__description
                    }
                  >
                    <h4>{product.name}</h4>
                    <span
                      className={
                        styles.MainPage__popular__carouselItem__description__price
                      }
                    >
                      {product.price} €
                    </span>
                  </div>
                </div>
              </a>
            </Link>
          );
        })}
      </>
    );
  };

  const displayProduct = () => {
    return (
      <>
        {products.map((product) => {
          return (
            <Link href={`/home/${product.id}`} key={product.id}>
              <a className={styles.MainPage__other__card__item}>
                <Card
                  src={product.imageUrl}
                  alt={product.name}
                  title={product.name}
                  description={product.description}
                  price={product.price}
                />
              </a>
            </Link>
          );
        })}
      </>
    );
  };

  useEffect(() => {
    if (!catActive) return;

    if (catActive === 'Tous') fetchProducts();
    else {
      fetchProductsByCat();
    }
  }, [catActive]);

  useEffect(() => {
    fetchCategories();
  }, []);

  return (
    <div className={styles.MainPage}>
      {loading ? (
        <Spinner loading={loading} color="#8190f5" />
      ) : (
        <>
          <div className={styles.MainPage__coupon}>
            <img src="/images/getcoupon.png" alt="Picture of the author" />
            <div className={styles.MainPage__coupon__content}>
              <p>Nouveau sur l’application ?</p>
              <button>Avoir un Coupon</button>
            </div>
          </div>

          <Carousel className={styles.MainPage__filters}>
            <Button
              className={styles.MainPage__filters__item}
              active={
                catActive === 'Tous'
                  ? styles.MainPage__filters__item__active
                  : ''
              }
              onClick={() => setCatActive('Tous')}
            >
              Tous
            </Button>
            {displayCategory()}
          </Carousel>

          <div className={styles.MainPage__popular}>
            <div className={styles.MainPage__popular__header}>
              <h3>Populaire</h3>
              <span className={styles.MainPage__popular__header__more}>
                <p>Voir tout</p>
                <Button className={styles.MainPage__popular__header__more__btn}>
                  {' >'}
                </Button>
              </span>
            </div>

            <Carousel>{displayPopularProduct()}</Carousel>
          </div>

          <div className={styles.MainPage__other}>
            <div className={styles.MainPage__other__header}>
              <h3>Autres</h3>
              <span className={styles.MainPage__other__header__more}>
                <p>Voir tout</p>
                <Button className={styles.MainPage__other__header__more__btn}>
                  {' >'}
                </Button>
              </span>
            </div>

            <div className={styles.MainPage__other__card}>
              {displayProduct()}
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default MainPage;
