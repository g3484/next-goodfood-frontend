import { faReceipt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Button from '../../components/button/button';
import styles from './DescriptionOrderPage.module.scss';
import { useRouter } from 'next/router';

interface DescriptionOrderPageProps {
  order: {
    id: string;
    userId: string;
    status: string;
    products: [
      {
        _id: string;
        name: string;
        imageUrl: string;
      }
    ];
    dateOfOrder: string;
    price: number;
    version: number;
  };
}

const DescriptionOrderPage = ({ order }: DescriptionOrderPageProps) => {
  const router = useRouter();

  const formatDate = (date: string) => {
    const orderDate = new Date(date);
    const day =
      orderDate.getDate() < 10
        ? '0' + orderDate.getDate()
        : orderDate.getDate();
    const month =
      orderDate.getMonth() + 1 < 10
        ? '0' + orderDate.getMonth() + 1
        : orderDate.getMonth() + 1;
    const year = orderDate.getFullYear();

    const hours =
      orderDate.getHours() < 10
        ? '0' + orderDate.getHours()
        : orderDate.getHours();
    const minutes =
      orderDate.getMinutes() < 10
        ? '0' + orderDate.getMinutes()
        : orderDate.getMinutes();

    return `${day}/${month}/${year} à ${hours}h${minutes}`;
  };

  const displayProducts = () => {
    console.log(order, 'orders');
    const productsFlat = order.products.flat();

    console.log(productsFlat, 'flat');

    const productsCount = productsFlat.reduce(
      (
        acc: {
          [key: string]: {
            id: string;
            name: string;
            imageUrl: string;
            count: number;
          };
        },
        product
      ) => {
        if (acc[product._id]) {
          acc[product._id].count++;
        } else {
          acc[product._id] = {
            id: product._id,
            name: product.name,
            imageUrl: product.imageUrl,
            count: 1,
          };
        }
        return acc;
      },
      {}
    );

    console.log(productsCount, 'count');

    return Object.values(productsCount).map((product) => (
      <div className={styles.DescriptionOrderPage__divRecap} key={product.id}>
        <span className={styles.DescriptionOrderPage__nbProduct}>
          {product.count}
        </span>
        <div className={styles.DescriptionOrderPage__divRecapOrder}>
          <h4>{product.name}</h4>
        </div>
      </div>
    ));
  };

  return (
    <div className={styles.DescriptionOrderPage}>
      <img
        src="/svg/icn-arrowLeft.svg"
        alt=""
        onClick={() => router.push('/orders')}
        className={styles.DescriptionOrderPage__arrow}
      />

      <h2 className={styles.DescriptionOrderPage__id}>
        Commande #{order.id.slice(0, 10)}
      </h2>
      <img
        src={order.products[0].imageUrl}
        alt={order.products[0].name}
        className={styles.DescriptionOrderPage__headerImg}
      />
      <h1 className={styles.DescriptionOrderPage__titleNameResto}>
        The Good Food - Rive Droite
      </h1>
      <p className={styles.DescriptionOrderPage__statusDate}>
        <span className={styles.DescriptionOrderPage__statusDate__status}>
          {order.status}
        </span>

        <span className={styles.DescriptionOrderPage__statusDate__date}>
          {formatDate(order.dateOfOrder)}
        </span>
      </p>
      <h3>Votre Commande</h3>
      <div className={styles.DescriptionOrderPage__divOrder}>
        {displayProducts()}
      </div>

      <p className={styles.DescriptionOrderPage__pDescLivraison}>
        <FontAwesomeIcon
          icon={faReceipt}
          className={styles.DescriptionOrderPage__iconReceipt}
        />
        <span>Total : {order.price} €</span>
      </p>

      <Button className={styles.DescriptionOrderPage__receipt}>
        Voir le reçu
      </Button>
    </div>
  );
};

export default DescriptionOrderPage;
