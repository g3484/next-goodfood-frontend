import React from 'react';
import styles from './SignInWith.module.scss';

const SignInWith = () => {
  return (
    <div className={styles.SignInWith}>
      <div className={styles.SignInWith__label}>
        <div className={styles.SignInWith__label__separator}></div>
        <p>Ou continuer avec</p>
        <div className={styles.SignInWith__label__separator}></div>
      </div>
      <div className={styles.SignInWith__social}>
        <img src={`/svg/apple.svg`} alt="" />
        <img src={`/svg/google.svg`} alt="" />
        <img src={`/svg/github.svg`} alt="" />
      </div>
    </div>
  );
};

export default SignInWith;
