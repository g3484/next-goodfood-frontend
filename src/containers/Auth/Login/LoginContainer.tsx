import React from 'react';
import Link from 'next/link';
import BackArrow from '../../../components/BackArrow/BackArrow';
import HeadTitle from '../HeadTitle/HeadTitle';
import SignInWith from '../SignInWith/SignInWith';
import styles from './LoginContainer.module.scss';
import Form from '../../../components/form/form';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import IFormLogin from '../../../interfaces/login/IFormLogin.interface';
import loginSchema from '../../../validation/login/login.schema';
import { inputLoginData } from '../../../data/login/login.data';
import { getCurrentUser, login } from '../../../services/axios/authRequest';
import { useRouter } from 'next/router';
import jwtDecode from 'jwt-decode';
import useCurrentUserStore from '../../../store/currentUserStore';

interface TokenDto {
  id: string;
  email: string;
  exp: number;
  iat: number;
}

const LoginContainer = () => {
  // const [errors, setErrors] = useState(null);
  const router = useRouter();
  const { setCurrentUser } = useCurrentUserStore();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormLogin>({
    resolver: yupResolver(loginSchema),
  });

  const onSubmit = async (formData: IFormLogin) => {
    const { email, password } = formData;

    const mailAddress = email;

    const {
      data,
      status,
    }: {
      data: {
        jwt: string;
        refreshToken: string;
      };
      status: number;
    } = await login('/api/users/login', { mailAddress, password });

    if (status === 200) {
      const { jwt, refreshToken } = data;

      localStorage.setItem('access_token', jwt);
      localStorage.setItem('refresh_token', refreshToken);

      await fetchCurrentUser(jwt);

      router.push('/home');
    }

    if (status !== 200) {
      alert('error');
    }
  };

  const fetchCurrentUser = async (accessToken) => {
    try {
      const user = jwtDecode(accessToken) as TokenDto;
      const userId = user.id;

      const {
        data,
        status,
      }: {
        data: any;
        status: number;
      } = await getCurrentUser(`/api/users/profile/${userId}`);

      if (status === 200) {
        const { user, address } = data;
        setCurrentUser(user, address);
      }

      if (status === 401) {
        return router.push('/login');
      }

      if (status !== 200) {
        alert('error');
      }
    } catch (err) {
      return;
    }
  };

  return (
    <div className={styles.LoginContainer}>
      <BackArrow />

      <div className={styles.LoginContainer__image}>
        <img src="/svg/logo.svg" />
      </div>

      <div className={styles.LoginContainer__form}>
        <HeadTitle title={'Connexion'} subtitle={''} />

        <Form
          inputs={inputLoginData}
          btnLabel={'Connexion'}
          register={register}
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
          errors={errors}
          inputsClassName={styles.LoginContainer__inputs}
          btnClassName={styles.LoginContainer__btn}
        />

        <SignInWith />
        <p className={styles.LoginContainer__register}>
          Vous avez déja un compte ?
          <Link href="/register">
            <a> Inscription</a>
          </Link>
        </p>
      </div>
    </div>
  );
};

export default LoginContainer;
