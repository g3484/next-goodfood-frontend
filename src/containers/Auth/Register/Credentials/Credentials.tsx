import React from 'react';
import styles from './Credentials.module.scss';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import IFormRegister from '../../../../interfaces/register/IFormRegister.interface';
import registerCredentialsSchema from '../../../../validation/register/register-credentials.schema';
import { inputRegisterCredentialsData } from '../../../../data/register/register-credentials.data';
import Form from '../../../../components/form/form';
import INames from '../../../../interfaces/register/INames.interface';

const Credentials = ({ onSubmit }: INames) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormRegister>({
    resolver: yupResolver(registerCredentialsSchema),
  });

  return (
    <div className={styles.Credentials}>
      <Form
        inputs={inputRegisterCredentialsData}
        btnLabel={'Suivant'}
        register={register}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        errors={errors}
        inputsClassName={styles.Credentials__inputs}
        btnClassName={styles.Credentials__btn}
      />
    </div>
  );
};

export default Credentials;
