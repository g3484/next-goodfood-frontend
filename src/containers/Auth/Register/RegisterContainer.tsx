import React, { useRef, useState } from 'react';
import Link from 'next/link';
import BackArrow from '../../../components/BackArrow/BackArrow';
import HeadTitle from '../HeadTitle/HeadTitle';
import SignInWith from '../SignInWith/SignInWith';
import styles from './RegisterContainer.module.scss';

import IFormRegister from '../../../interfaces/register/IFormRegister.interface';
import Names from './Names/Names';
import Credentials from './Credentials/Credentials';
import Address from './Address/Address';
import MailSent from './MailSent/MailSent';
import { register } from '../../../services/axios/authRequest';

const RegisterContainer = () => {
  const [step, setStep] = useState(1);
  const [userName, setUserName] = useState('');
  const form = useRef<IFormRegister>({
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    password: '',
    confirmPassword: '',
    address: '',
    city: '',
    zipCode: '',
  });

  const onSubmitNames = (data: IFormRegister) => {
    console.log(data);
    form.current = {
      ...form.current,
      firstName: data.firstName,
      lastName: data.lastName,
    };
    setUserName(data.firstName);
    setStep(2);
  };

  const onSubmitCredentials = (data: IFormRegister) => {
    console.log(data);
    form.current = {
      ...form.current,
      email: data.email,
      phone: data.phone,
      password: data.password,
      confirmPassword: data.confirmPassword,
    };
    setStep(3);
  };

  const onSubmitAddress = async (data: IFormRegister) => {
    console.log(data);
    form.current = {
      ...form.current,
      address: data.address,
      city: data.city,
      zipCode: data.zipCode,
    };

    console.log(form.current);

    const { status } = await register('/api/users/registration', {
      user: {
        firstname: form.current.firstName,
        lastname: form.current.lastName,
        phone: form.current.phone,
        mailAddress: form.current.email,
        password: form.current.password,
      },
      address: {
        lane: form.current.address,
        zipCode: form.current.zipCode,
        city: form.current.city,
      },
    });

    if (status === 201) {
      setStep(4);
    }

    if (status !== 201) {
      alert('error');
    }
  };

  return (
    <div className={styles.RegisterContainer}>
      <BackArrow />
      <div className={styles.RegisterContainer__image}>
        <img src="/svg/logo.svg" />
      </div>

      <div className={styles.RegisterContainer__form}>
        {step !== 4 ? (
          <>
            <HeadTitle
              title={'Bonjour,'}
              subtitle={userName ? userName : 'Présente toi !'}
            />

            {step === 1 && <Names onSubmit={onSubmitNames} />}
            {step === 2 && <Credentials onSubmit={onSubmitCredentials} />}
            {step === 3 && <Address onSubmit={onSubmitAddress} />}

            <SignInWith />
            <p className={styles.RegisterContainer__form__register}>
              Vous avez déja un compte ?
              <Link href="/login">
                <a> Inscription</a>
              </Link>
            </p>
          </>
        ) : (
          <MailSent />
        )}
      </div>
    </div>
  );
};

export default RegisterContainer;
