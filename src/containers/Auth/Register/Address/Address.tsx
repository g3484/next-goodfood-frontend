import React from 'react';
import styles from './Address.module.scss';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import IFormRegister from '../../../../interfaces/register/IFormRegister.interface';
import registerAddressSchema from '../../../../validation/register/register-address.schema';
import { inputRegisterAddressData } from '../../../../data/register/register-address.data';
import Form from '../../../../components/form/form';
import INames from '../../../../interfaces/register/INames.interface';

const Address = ({ onSubmit }: INames) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormRegister>({
    resolver: yupResolver(registerAddressSchema),
  });

  return (
    <div className={styles.Address}>
      <Form
        inputs={inputRegisterAddressData}
        btnLabel={'Inscription'}
        register={register}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        errors={errors}
        inputsClassName={styles.Address__inputs}
        btnClassName={styles.Address__btn}
      />
    </div>
  );
};

export default Address;
