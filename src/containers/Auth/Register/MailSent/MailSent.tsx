import React from 'react';
import styles from './MailSent.module.scss';

const MailSent = () => {
  return (
    <div className={styles.MailSent}>
      <img src="/svg/mail_sent.svg" alt="" />
      <p>Un email de confirmation vous a été envoyé</p>
    </div>
  );
};

export default MailSent;
