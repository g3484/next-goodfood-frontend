import React from 'react';
import styles from './Names.module.scss';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import IFormRegister from '../../../../interfaces/register/IFormRegister.interface';
import registerNameSchema from '../../../../validation/register/register-names.schema';
import { inputRegisterNamesData } from '../../../../data/register/register-names.data';
import Form from '../../../../components/form/form';
import INames from '../../../../interfaces/register/INames.interface';

const Names = ({ onSubmit }: INames) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormRegister>({
    resolver: yupResolver(registerNameSchema),
  });

  return (
    <div className={styles.Names}>
      <Form
        inputs={inputRegisterNamesData}
        btnLabel={'Suivant'}
        register={register}
        handleSubmit={handleSubmit}
        onSubmit={onSubmit}
        errors={errors}
        inputsClassName={styles.Names__inputs}
        btnClassName={styles.Names__btn}
      />
    </div>
  );
};

export default Names;
