import React from 'react';
import PropTypes from 'prop-types';
import styles from './HeadTitle.module.scss';

const HeadTitle = ({ title, subtitle }) => {
  return (
    <div className={styles.HeadTitle}>
      <h2 className={styles.HeadTitle__title}>{title}</h2>
      <p className={styles.HeadTitle__subTitle}>{subtitle}</p>
    </div>
  );
};

HeadTitle.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

export default HeadTitle;
