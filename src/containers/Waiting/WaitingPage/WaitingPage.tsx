import React from 'react';
import styles from './WaitingPage.module.scss';
import { useRouter } from 'next/router';

const WaitingPage = () => {
  const router = useRouter();
  return (
    <div className={styles.WaitingPage}>
      <img
        src="/svg/icn-arrowLeft.svg"
        alt=""
        onClick={() => router.push('/orders')}
        className={styles.WaitingPage__arrow}
      />

      <div className={styles.WaitingPage__description}>
        <img
          src="/images/waiting.png"
          alt="Picture of the author"
          className={styles.WaitingPage__description__img}
        />
        <p className={styles.WaitingPage__description__OrderWaiting}>
          Commande en cours de préparation ...
        </p>
      </div>
    </div>
  );
};

export default WaitingPage;
