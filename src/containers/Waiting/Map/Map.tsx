import React, { useRef, useEffect, useState } from 'react';
import mapboxgl from 'mapbox-gl';
import styles from './Map.module.scss';
import Link from 'next/link';
import ButtonIcone from '../../../components/buttonIcone/buttonIcone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { getDelivery } from '../../../services/axios/deliveryRequest';
import { useRouter } from 'next/router';
import axios from 'axios';
import Spinner from '../../../components/Spinner/Spinner';

interface MapProps {
  orderId: string;
}

const Map = ({ orderId }: MapProps) => {
  const [loading, setLoading] = useState(true);
  const [clientCoords, setClientCoords] = useState([]);
  const router = useRouter();
  const map = useRef(null);
  const interval = useRef(null);
  const mapContainerRef = useRef(null);

  mapboxgl.accessToken =
    'pk.eyJ1IjoibW9yZ2FuZXZkIiwiYSI6ImNsNWwxZms1MzBmbXUzZHBhZGlmZ3loaG8ifQ.TBJIfi074iH50kjfK7n-kw';

  const drawPath = async () => {
    const { data, status } = await getDelivery(`/api/deliveries/${orderId}`);

    if (status === 200) {
      const delivererCoords = [
        data[0].delivererCoordinates.longitude,
        data[0].delivererCoordinates.latitude,
      ];

      await getRoute(delivererCoords);
    }
  };

  const transformAdressToCoordinates = async (address) => {
    const { data, status } = await axios.get(
      `https://api.mapbox.com/geocoding/v5/mapbox.places/${address}.json?access_token=${mapboxgl.accessToken}`
    );

    if (status === 200) {
      if (!data.features.length) {
        alert('Adresse non valide');
        return;
      }

      setClientCoords(data.features[0].center);
      return data.features[0].center;
    }

    if (status !== 200) {
      alert('error');
    }
  };

  const getRoute = async (delivererCoords) => {
    const { data, status } = await axios.get(
      `https://api.mapbox.com/directions/v5/mapbox/cycling/${delivererCoords[0]},${delivererCoords[1]};${clientCoords[0]},${clientCoords[1]}?steps=true&geometries=geojson&access_token=${mapboxgl.accessToken}`
    );

    if (status === 200) {
      const route = data.routes[0].geometry.coordinates;

      const geojson = {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'LineString',
          coordinates: route,
        },
      };

      // if the route already exists on the map, we'll reset it using setData
      if (map.current.getSource('route')) {
        map.current.getSource('route').setData(geojson);
      } else {
        map.current.addLayer({
          id: 'route',
          type: 'line',
          source: {
            type: 'geojson',
            data: geojson,
          },
          layout: {
            'line-join': 'round',
            'line-cap': 'round',
          },
          paint: {
            'line-color': '#3887be',
            'line-width': 5,
            'line-opacity': 0.75,
          },
        });
      }

      const instructions = document.getElementById('instructions');
      instructions.innerHTML = `<p>
        <strong>${Math.floor(data.routes[0].duration / 60)} min 🚴- </strong>
        <strong>${(data.routes[0].distance / 1000).toFixed(2)} km </strong>
      </p>`;

      // Start
      if (map.current.getSource('start')) {
        console.log('update start');

        map.current.getSource('start').setData({
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: delivererCoords,
              },
            },
          ],
        });
      } else {
        console.log('new start');

        map.current.loadImage(
          'https://res.cloudinary.com/dxnet6slc/image/upload/v1657818858/bike_yqxptl.png',
          (error, image) => {
            if (error) throw error;

            map.current.addImage('bike', image);

            map.current.addLayer({
              id: 'start',
              type: 'symbol',
              source: {
                type: 'geojson',
                data: {
                  type: 'FeatureCollection',
                  features: [
                    {
                      type: 'Feature',
                      geometry: {
                        type: 'Point',
                        coordinates: delivererCoords,
                      },
                    },
                  ],
                },
              },
              layout: {
                'icon-image': 'bike',
                'icon-size': 0.07,
              },
            });
          }
        );
      }

      // End
      if (map.current.getSource('end')) {
        console.log('update end');
        map.current.getSource('end').setData({
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Point',
                coordinates: clientCoords,
              },
            },
          ],
        });
      } else {
        console.log('new end');

        map.current.addLayer({
          id: 'end',
          type: 'circle',
          source: {
            type: 'geojson',
            data: {
              type: 'FeatureCollection',
              features: [
                {
                  type: 'Feature',
                  properties: {},
                  geometry: {
                    type: 'Point',
                    coordinates: clientCoords,
                  },
                },
              ],
            },
          },
          paint: {
            'circle-radius': 10,
            'circle-color': '#f30',
          },
        });
      }

      map.current.flyTo({
        center: delivererCoords,
      });

      setLoading(false);
    }

    if (status !== 200) {
      console.log(data);
      alert('error');
    }
  };

  const getClientCoord = async () => {
    const { data, status } = await getDelivery(`/api/deliveries/${orderId}`);

    if (status === 200) {
      const coord = await transformAdressToCoordinates(data[0].clientAddress);
      setClientCoords(coord);
    }

    if (status === 401) {
      router.push('/login');
    }

    if (status !== 200) {
      alert('error');
    }
  };

  // Display map
  useEffect(() => {
    map.current = new mapboxgl.Map({
      container: mapContainerRef.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [1.099971, 49.443232], // starting position
      zoom: 12,
    });

    return () => map.current.remove();
  }, []);

  // Get client coordinates
  useEffect(() => {
    if (!orderId) return;

    getClientCoord();
  }, [orderId]);

  // Draw path
  useEffect(() => {
    if (!clientCoords.length) return;

    drawPath();

    interval.current = setInterval(() => {
      drawPath();
    }, 30000);

    return () => clearInterval(interval.current);
  }, [clientCoords]);

  return (
    <div className={styles.Map}>
      {loading && <Spinner loading={loading} color="#0B70B5" />}

      <div
        className={styles.Map__container}
        style={loading ? { opacity: 0 } : { opacity: 1 }}
      >
        <div
          className={styles.Map__container__mapbox}
          ref={mapContainerRef}
        ></div>
        <div className={styles.Map__container__instructions}>
          <Link href="/orders">
            <a>
              <ButtonIcone className={styles.Map__container__btnRetour}>
                <FontAwesomeIcon icon={faArrowLeft} />
              </ButtonIcone>
            </a>
          </Link>
          <div id="instructions"></div>
        </div>
      </div>
    </div>
  );
};

export default Map;
