import React, { useEffect } from 'react';
import styles from './OrderPage.module.scss';
import ButtonIcone from '../../components/buttonIcone/buttonIcone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import Carousel from '../../components/carousel/carousel';
import Link from 'next/link';

const OrderPage = () => {
  useEffect(() => {}, []);

  return (
    <div className={styles.OrderPage}>
      <div className={styles.OrderPage__title}>
        <Link href="/home">
          <a>
            <ButtonIcone className={styles.OrderPage__btnRetour}>
              <FontAwesomeIcon icon={faArrowLeft} />
            </ButtonIcone>
          </a>
        </Link>
        <h1>Vérifiez votre commande</h1>
        <Link href="/description-order">
          <a>
            <ButtonIcone className={styles.OrderPage__btnInfo}>
              <FontAwesomeIcon
                icon={faInfoCircle}
                className={styles.OrderPage__iconBtn}
              />
            </ButtonIcone>
          </a>
        </Link>
      </div>
      <div className={styles.OrderPage__divBtnNote}>
        <div className={styles.OrderPage__divTitleNote}>
          <ButtonIcone className={styles.OrderPage__btnNote}>
            <img src="../../svg/bad.png" />
          </ButtonIcone>
          <h2>Mauvais</h2>
        </div>

        <div className={styles.OrderPage__divTitleNote}>
          <ButtonIcone className={styles.OrderPage__btnNote}>
            <img src="../../svg/good.png" />
          </ButtonIcone>
          <h2>Bien</h2>
        </div>
        <div className={styles.OrderPage__divTitleNote}>
          <ButtonIcone className={styles.OrderPage__btnNote}>
            <img src="../../svg/amazing.png" />
          </ButtonIcone>
          <h2>Super !</h2>
        </div>
      </div>
      <Carousel>
        <img
          src="http://res.cloudinary.com/morgane-vauchel-durel/image/upload/v1650989838/2022-04-26_18:17_image.jpg"
          alt="Parmentier"
          className={styles.OrderPage__carouselImg}
        />

        <img
          src="http://res.cloudinary.com/morgane-vauchel-durel/image/upload/v1651073881/2022-04-27_17:38_image.jpg"
          alt="CheeseBurger"
          className={styles.OrderPage__carouselImg}
        />

        <img
          src="http://res.cloudinary.com/morgane-vauchel-durel/image/upload/v1651075827/2022-04-27_18:10_image.webp"
          alt="Salade niçoise"
          className={styles.OrderPage__carouselImg}
        />

        <img
          src="http://res.cloudinary.com/morgane-vauchel-durel/image/upload/v1650988724/2022-04-26_17:58_image.png"
          alt="Asperges"
          className={styles.OrderPage__carouselImg}
        />
        <img
          src="http://res.cloudinary.com/morgane-vauchel-durel/image/upload/v1651077466/2022-04-27_18:37_image.jpg"
          alt="PizzaPeperonni"
          className={styles.OrderPage__carouselImg}
        />
      </Carousel>
    </div>
  );
};

export default OrderPage;
