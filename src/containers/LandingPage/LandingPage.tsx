import React from 'react';
import Button from '../../components/button/button';
import styles from './LandingPage.module.scss';

const LandingPage = () => {
  return (
    <div className={styles.LandingPage}>
      <div className={styles.LandingPage__image}>
        <img src="/svg/logo.svg" />
      </div>

      <div className={styles.LandingPage__content}>
        <p>
          Bienvenue sur GoodFood notre plateforme de e-commerce où vous pouvez
          réaliser votre commande des produits que nous disposons et être livré
          chez vous en quelques minutes.
        </p>

        <div className={styles.LandingPage__content__btnContainer}>
          <Button
            className={
              styles.LandingPage__content__btnContainer__btnInscription
            }
            href="/register"
          >
            Inscription
          </Button>
          <Button
            className={styles.LandingPage__content__btnContainer__btnConnexion}
            href="/login"
          >
            Connexion
          </Button>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
