import React from 'react';
import styles from './CheckoutPage.module.scss';
import StripeCheckout from 'react-stripe-checkout';
import Spinner from '../../components/Spinner/Spinner';
import { createPayment } from '../../services/axios/paymentRequest';
import { useRouter } from 'next/router';
import useCurrentUserStore from '../../store/currentUserStore';
import { cancelOrder } from '../../services/axios/orderRequest';
import { useCartStore } from '../../store/cartStore';

const CheckoutPage = ({ order }) => {
  const router = useRouter();
  const { user, address } = useCurrentUserStore();

  const { clearCart } = useCartStore();

  const onToken = async (token) => {
    const { id } = token;

    const {
      data,
      status,
    }: {
      data: any;
      status: number;
    } = await createPayment(`/api/payments`, {
      token: id,
      orderId: order.id,
    });

    if (status === 201) {
      console.log(data, 'payment');
      clearCart();
      return router.push(`/orders/delivery/${order.id}`);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 201) {
      alert('error');
    }
  };

  const deleteOrder = async () => {
    const {
      status,
    }: {
      status: number;
    } = await cancelOrder(`/api/orders/${order.id}`);

    if (status === 204) {
      router.push('/cart');
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 204) {
      alert('error');
    }
  };

  if (!user) {
    return <Spinner loading={true} color="#8190f5" />;
  }

  return (
    <div className={styles.CheckoutPage}>
      {!order ? (
        <Spinner loading={true} color="#8190f5" />
      ) : (
        <>
          <img src="/svg/icn-arrowLeft.svg" alt="" onClick={deleteOrder} />

          <div className={styles.CheckoutPage__location}>
            <h2>Adresse</h2>

            <div className={styles.CheckoutPage__location__address}>
              <img src="/images/temp-location.png" alt="" />

              <div className={styles.CheckoutPage__location__address__content}>
                <h3>Domicile</h3>
                <p>{address.lane}</p>
                <p>
                  {address.zipCode}, {address.city}
                </p>
              </div>
            </div>
          </div>

          <div className={styles.CheckoutPage__price}>
            <div className={styles.CheckoutPage__price__item}>
              <p>Articles</p>
              <p>{order.price} €</p>
            </div>
            <div className={styles.CheckoutPage__price__total}>
              <p>Total</p>
              <p>{order.price} €</p>
            </div>
          </div>

          <StripeCheckout
            token={onToken}
            stripeKey="pk_test_51LZIjcLKn7Ng6PU3JelE3K7V0dmGAUhpAxT66BeWMwOIeXGJxlCkSZnkupXd453tSU0Vb9c6mg16Y0tR30syfgUW00TXUXI2DG"
            amount={order.price * 100}
            email={user.mailAddress.trim()}
          />
        </>
      )}
    </div>
  );
};

export default CheckoutPage;
