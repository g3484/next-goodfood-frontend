import React from 'react';
import styles from './ProductPage.module.scss';
import ButtonIcone from '../../components/buttonIcone/buttonIcone';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import Button from '../../components/button/button';
import { useCartStore } from '../../store/cartStore';

interface ProductPageProps {
  product: {
    id: string;
    name: string;
    imageUrl: string;
    price: number;
    description: string;
    idCategory: string;
  };
}

const ProductPage = ({ product }: ProductPageProps) => {
  const { addToCart } = useCartStore();

  const handleAddToCart = () => {
    addToCart({
      id: product.id,
      name: product.name,
      image: product.imageUrl,
      price: product.price,
      quantity: 1,
    });

    console.log('added to cart');
  };

  return (
    <div className={styles.ProductPage}>
      <div className={styles.ProductPage__title}>
        <Link href="/home">
          <a>
            <ButtonIcone className={styles.ProductPage__btnRetour}>
              <FontAwesomeIcon icon={faArrowLeft} />
            </ButtonIcone>
          </a>
        </Link>
      </div>
      <div className={styles.ProductPage__description}>
        <img
          src={product.imageUrl}
          alt={product.name}
          className={styles.ProductPage__description__img}
        />
        <h1>{product.name}</h1>
        <p className={styles.ProductPage__description__detailsProduit}>
          {product.description}
        </p>
        <span className={styles.ProductPage__description__price}>
          {product.price} €
        </span>
      </div>
      <Button
        className={styles.ProductPage__btnAddToCart}
        onClick={handleAddToCart}
      >
        Ajouter au panier
      </Button>
    </div>
  );
};

export default ProductPage;
