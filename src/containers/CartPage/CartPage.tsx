import React from 'react';
import Button from '../../components/button/button';
import styles from './CartPage.module.scss';
import { useCartStore } from '../../store/cartStore';
import useCurrentUserStore from '../../store/currentUserStore';
import { createOrder } from '../../services/axios/orderRequest';
import { useRouter } from 'next/router';

const CartPage = () => {
  const { cart, addToCart, removeOne, getAllQuantity, getTotalPrice } =
    useCartStore();

  const { user, address } = useCurrentUserStore();

  const router = useRouter();

  const getProductIds = () => {
    const ids = [];

    cart.map((item) => {
      for (let i = 0; i < item.quantity; i++) {
        ids.push(item.id);
      }
    });

    return ids;
  };

  const handleCheckout = async () => {
    const {
      data,
      status,
    }: {
      data: any;
      status: number;
    } = await createOrder('/api/orders', {
      productIds: getProductIds(),
      userName: `${user.firstname} ${user.lastname}`,
      address: `${address.lane} ${address.zipCode} ${address.city}`,
    });

    if (status === 201) {
      return router.push(`/orders/checkout/${data.id}`);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 201) {
      alert('Something went wrong');
    }
  };

  return (
    <div className={styles.CartPage}>
      {!cart.length ? (
        <p className={styles.CartPage__empty}>Your cart is empty</p>
      ) : (
        <>
          <div className={styles.CartPage__header}>
            <p>{getAllQuantity()} Articles</p>
            <Button className={styles.CartPage__header__more} href="/home">
              Ajouter plus
            </Button>
          </div>

          <div className={styles.CartPage__list}>
            {cart.map((item) => (
              <div className={styles.CartPage__list__item} key={item.id}>
                <img
                  className={styles.CartPage__list__item__image}
                  src={item.image}
                  alt={item.name}
                />

                <div className={styles.CartPage__list__item__info}>
                  <p>{item.name}</p>
                  <p>{item.price} €</p>
                </div>
                <div className={styles.CartPage__list__item__quantity}>
                  <Button
                    className={`${styles.CartPage__list__item__quantity__controls} ${styles.CartPage__list__item__quantity__controls__minus}`}
                    onClick={() => removeOne(item.id)}
                  >
                    -
                  </Button>
                  <p>{item.quantity}</p>

                  <Button
                    className={`${styles.CartPage__list__item__quantity__controls} ${styles.CartPage__list__item__quantity__controls__plus}`}
                    onClick={() => addToCart(item)}
                  >
                    +
                  </Button>
                </div>
              </div>
            ))}
          </div>

          <div className={styles.CartPage__price}>
            <div className={styles.CartPage__price__subTotal}>
              <p>Sous-total</p>
              <p>{getTotalPrice()} €</p>
            </div>
            <div className={styles.CartPage__price__total}>
              <p>Total</p>
              <p>{getTotalPrice()} €</p>
            </div>
          </div>

          <Button
            className={styles.CartPage__checkout}
            onClick={handleCheckout}
          >
            Accéder au paiement
          </Button>
        </>
      )}
    </div>
  );
};

export default CartPage;
