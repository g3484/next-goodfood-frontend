import create from 'zustand';
import { persist } from 'zustand/middleware';

export interface CartItem {
  id: string;
  name: string;
  image: string;
  price: number;
  quantity: number;
}

export interface CartStore {
  cart: CartItem[];
  addToCart: (item: CartItem) => void;
  removeFromCart: (id: string) => void;
  removeOne: (id: string) => void;
  clearCart: () => void;
  getAllQuantity: () => number;
  getTotalPrice: () => number;
}

export const useCartStore = create<CartStore>()(
  persist(
    (set, get) => ({
      cart: [],

      addToCart: (item) => {
        const cart = get().cart;
        const existingItem = cart.find((cartItem) => cartItem.id === item.id);
        if (existingItem) {
          existingItem.quantity++;
        } else {
          cart.push(item);
        }
        set({ cart });
      },

      removeFromCart: (id) => {
        const cart = get().cart;
        const index = cart.findIndex((cartItem) => cartItem.id === id);
        if (index > -1) {
          cart.splice(index, 1);
        }
        set({ cart });
      },

      removeOne: (id) => {
        const cart = get().cart;
        const existingItem = cart.find((cartItem) => cartItem.id === id);
        if (existingItem) {
          existingItem.quantity--;
          if (existingItem.quantity === 0) {
            get().removeFromCart(id);
          }
        }
        set({ cart });
      },

      clearCart: () => {
        set({ cart: [] });
      },

      getAllQuantity: () => {
        const cart = get().cart;
        return cart.reduce((acc, item) => acc + item.quantity, 0);
      },

      getTotalPrice: () => {
        const cart = get().cart;
        return cart.reduce((acc, item) => acc + item.quantity * item.price, 0);
      },
    }),
    {
      name: 'cart',
      getStorage: () => localStorage,
    }
  )
);
