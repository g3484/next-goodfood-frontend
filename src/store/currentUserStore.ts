import create from 'zustand';

export interface userItem {
  id: string;
  lastname: string;
  firstname: string;
  mailAddress: string;
  avatar: string;
  roles: string[];
  phone: string;
}

export interface addressItem {
  id: string;
  userId: string;
  lane: string;
  zipCode: string;
  city: string;
}

const useCurrentUserStore = create<{
  user: userItem;
  address: addressItem;
  setUser: (user: userItem) => void;
  setAddress: (address: addressItem) => void;
  setCurrentUser: (user: userItem, address: addressItem) => void;
}>((set, get) => ({
  user: {
    id: '',
    lastname: '',
    firstname: '',
    mailAddress: '',
    avatar: '',
    roles: [],
    phone: '',
  },

  address: {
    id: '',
    userId: '',
    lane: '',
    zipCode: '',
    city: '',
  },

  setUser: (user: userItem) => {
    const { id, mailAddress, firstname, lastname, phone, avatar, roles } = user;
    const trimmedRoles = roles.map((role) => role.trim());

    user = {
      id: id.trim(),
      lastname: lastname.trim(),
      firstname: firstname.trim(),
      mailAddress: mailAddress.trim(),
      avatar: avatar ? avatar.trim() : '',
      roles: trimmedRoles,
      phone: phone.trim(),
    };
    set({ user });
  },

  setAddress: (address: addressItem) => {
    const { id: addressId, userId, lane, zipCode, city } = address;

    address = {
      id: addressId,
      userId: userId.trim(),
      lane: lane.trim(),
      zipCode: zipCode.trim(),
      city: city.trim(),
    };
    set({ address });
  },

  setCurrentUser: (user: userItem, address: addressItem) => {
    const { id, mailAddress, firstname, lastname, phone, avatar, roles } = user;
    const { id: addressId, userId, lane, zipCode, city } = address;

    const trimmedRoles = roles.map((role) => role.trim());

    user = {
      id: id.trim(),
      lastname: lastname.trim(),
      firstname: firstname.trim(),
      mailAddress: mailAddress.trim(),
      avatar: avatar ? avatar.trim() : '',
      roles: trimmedRoles,
      phone: phone.trim(),
    };

    address = {
      id: addressId,
      userId: userId.trim(),
      lane: lane.trim(),
      zipCode: zipCode.trim(),
      city: city.trim(),
    };

    set({ user, address });
  },
}));

export default useCurrentUserStore;
