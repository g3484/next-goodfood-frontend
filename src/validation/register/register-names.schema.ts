import * as yup from 'yup';

const registerNamesSchema = yup.object({
  firstName: yup.string().required(),
  lastName: yup.string().required(),
});

export default registerNamesSchema;
