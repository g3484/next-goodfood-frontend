import * as yup from 'yup';

const registerAddressSchema = yup.object({
  address: yup.string().required(),
  city: yup.string().required(),
  zipCode: yup.string().required(),
});

export default registerAddressSchema;
