import * as yup from 'yup';

const registerCredentialsSchema = yup.object({
  email: yup.string().email().required(),
  phone: yup.string().required(),
  password: yup.string().min(8).max(20).required(),
  confirmPassword: yup.string().oneOf([yup.ref('password'), null]),
});

export default registerCredentialsSchema;
