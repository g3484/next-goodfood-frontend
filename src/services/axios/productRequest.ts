import axiosClient from './axiosClient';

/** PRODUCT REQUEST */

export const getProducts = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getSingleProduct = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getProductByCatId = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

/** CATEGORY REQUEST */

export const getCategories = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};
