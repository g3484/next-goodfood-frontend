import axiosClient from './axiosClient';

/** HEALTHY REQUEST */

export const isHealthy = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};
