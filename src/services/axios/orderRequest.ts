import axiosClient from './axiosClient';

/** ORDER REQUEST */

export const createOrder = async (URL: string, body: object) => {
  return await axiosClient
    .post(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getOrders = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getOrder = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const cancelOrder = async (URL: string) => {
  return await axiosClient
    .delete(URL)
    .then((response) => response)
    .catch((error) => error.response);
};
