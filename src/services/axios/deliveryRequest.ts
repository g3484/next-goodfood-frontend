import axiosClient from './axiosClient';

/** DELIVERY REQUEST */

export const getDelivery = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};
