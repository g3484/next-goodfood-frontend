import axiosClient from './axiosClient';

/** LOGIN REQUEST */

export const login = async (URL: string, body: object) => {
  return await axiosClient
    .post(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};

export const getCurrentUser = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};

export const register = async (URL: string, body: object) => {
  return await axiosClient
    .post(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};

export const confirmAccount = async (URL: string) => {
  return await axiosClient
    .get(URL)
    .then((response) => response)
    .catch((error) => error.response);
};
