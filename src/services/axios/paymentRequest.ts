import axiosClient from './axiosClient';

export const createPayment = async (URL: string, body: object) => {
  return await axiosClient
    .post(URL, body)
    .then((response) => response)
    .catch((error) => error.response);
};
