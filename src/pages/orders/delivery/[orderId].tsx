import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../../components/layout';
import { getOrder } from '../../../services/axios/orderRequest';
import WaitingPage from '../../../containers/Waiting/WaitingPage/WaitingPage';
import Map from '../../../containers/Waiting/Map/Map';
import Spinner from '../../../components/Spinner/Spinner';
import Head from 'next/head';

type Order = {
  id: string;
  userId: string;
  status: string;
  products: [];
  dateOfOrder: string;
  version: number;
};

interface OrderProps {
  orderId: string;
}

const Order = ({ orderId }: OrderProps) => {
  const [loading, setLoading] = useState(true);
  const [order, setOrder] = useState<Order>();
  const router = useRouter();

  const fetchOrder = async () => {
    const {
      data,
      status,
    }: {
      data: Order;
      status: number;
    } = await getOrder(`/api/orders/${orderId}`);

    if (status === 200) {
      setOrder(data);
      return setLoading(false);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      alert('error');
    }
  };

  useEffect(() => {
    fetchOrder();
  }, []);

  return (
    <Layout hasHeader={false}>
      <Head>
        <title>Livraison - Goodfood</title>
      </Head>

      {loading ? (
        <Spinner loading={loading} color="#8190f5" />
      ) : (
        <>
          {order.status === 'awaiting_delivery' && <WaitingPage />}
          {order.status === 'delivery_in_progress' && <Map orderId={orderId} />}
        </>
      )}
    </Layout>
  );
};

Order.getInitialProps = async ({ query }) => {
  return { orderId: query.orderId };
};

export default Order;
