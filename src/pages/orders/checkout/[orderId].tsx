import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '../../../components/layout';
import { getOrder } from '../../../services/axios/orderRequest';
import CheckoutPage from '../../../containers/CheckoutPage/CheckoutPage';
import Head from 'next/head';

type Order = {
  id: string;
  userId: string;
  status: string;
  products: any;
  dateOfOrder: string;
  version: number;
};

interface OrderProps {
  orderId: string;
}

const Order = ({ orderId }: OrderProps) => {
  const [order, setOrder] = useState<Order>();
  const router = useRouter();

  const fetchOrder = async () => {
    const {
      data,
      status,
    }: {
      data: Order;
      status: number;
    } = await getOrder(`/api/orders/${orderId}`);

    if (status === 200) {
      return setOrder(data);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      alert('error');
    }
  };

  useEffect(() => {
    fetchOrder();
  }, []);

  return (
    <Layout active="orders" hasHeader={false}>
      <Head>
        <title>Paiement - Goodfood</title>
      </Head>
      <CheckoutPage order={order} />
    </Layout>
  );
};

Order.getInitialProps = async ({ query }) => {
  return { orderId: query.orderId };
};

export default Order;
