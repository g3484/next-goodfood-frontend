import React, { useEffect, useState } from 'react';
import Layout from '../../components/layout';
import ListOrdersPage from '../../containers/ListOrdersPage/ListOrdersPage';
import { getOrders } from '../../services/axios/orderRequest';
import { useRouter } from 'next/router';
import Head from 'next/head';

type Order = {
  id: string;
  userId: string;
  status: string;
  products: [];
  dateOfOrder: string;
  version: number;
};

const Orders = () => {
  const [orders, setOrders] = useState<Order[]>([]);
  const router = useRouter();

  const fetchOrders = async () => {
    const {
      data,
      status,
    }: {
      data: Order[];
      status: number;
    } = await getOrders('/api/orders');

    if (status === 200) {
      return setOrders(data);
    }

    if (status === 401) {
      return router.push('/login');
    }

    if (status !== 200) {
      alert('error');
    }
  };

  useEffect(() => {
    fetchOrders();
  }, []);

  return (
    <Layout active="orders">
      <Head>
        <title>Mes commandes - Goodfood</title>
      </Head>
      <ListOrdersPage orders={orders} />
    </Layout>
  );
};

export default Orders;
