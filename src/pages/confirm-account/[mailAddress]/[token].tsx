/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import styles from './Token.module.scss';
import { useRouter } from 'next/router';
import {
  confirmAccount,
  getCurrentUser,
} from '../../../services/axios/authRequest';
import Spinner from '../../../components/Spinner/Spinner';
import useCurrentUserStore from '../../../store/currentUserStore';
import jwtDecode from 'jwt-decode';
import Head from 'next/head';

interface TokenDto {
  id: string;
  email: string;
  exp: number;
  iat: number;
}

const Confirm = () => {
  const [loading, setLoading] = useState(true);
  const router = useRouter();
  const { mailAddress, token } = router.query;

  const { setCurrentUser } = useCurrentUserStore();

  useEffect(() => {
    if (!mailAddress || !token) return;

    console.log(mailAddress, token);
    handleConfirmAccount();
  }, [mailAddress, token]);

  const fetchCurrentUser = async (accessToken) => {
    try {
      const user = jwtDecode(accessToken) as TokenDto;
      const userId = user.id;

      const {
        data,
        status,
      }: {
        data: any;
        status: number;
      } = await getCurrentUser(`/api/users/profile/${userId}`);

      if (status === 200) {
        const { user, address } = data;
        setCurrentUser(user, address);
      }

      if (status === 401) {
        return router.push('/login');
      }

      if (status !== 200) {
        alert('error');
      }
    } catch (err) {
      return;
    }
  };

  const handleConfirmAccount = async () => {
    const { data, status } = await confirmAccount(
      `/api/users/confirm/${mailAddress}/${token}`
    );

    if (status === 200) {
      setLoading(false);
      const { jwt, refreshToken } = data;

      localStorage.setItem('access_token', jwt);
      localStorage.setItem('refresh_token', refreshToken);

      await fetchCurrentUser(jwt);

      return setTimeout(() => {
        router.push('/home');
      }, 3000);
    }

    if (status !== 200) {
      alert('error');
    }
  };

  return (
    <div className={styles.Confirm}>
      <Head>
        <title>Confirmation de votre compte - Goodfood</title>
      </Head>
      {loading ? (
        <Spinner loading={loading} color="#8190f5" />
      ) : (
        <>
          <img src="/svg/confirm.svg" alt="" />

          <p>
            Votre compte a bien été validé <br />
            Vous allez être redirigé vers l'application
          </p>
        </>
      )}
    </div>
  );
};

export default Confirm;
