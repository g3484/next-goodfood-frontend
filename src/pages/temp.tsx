import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { isHealthy } from '../services/axios/tempRequest';

const Temp = () => {
  const [isHealthyMessage, setIsHealthyMessage] = useState(false);

  const fecthIsHealthy = async () => {
    const { data, status } = await isHealthy('/api/products/health');

    if (status === 200) {
      setIsHealthyMessage(data);
    }
  };

  useEffect(() => {
    fecthIsHealthy();
  }, []);

  return (
    <div>
      <Head>
        <title>Temp - Goodfood</title>
      </Head>
      <h1>{isHealthyMessage}</h1>
    </div>
  );
};

export default Temp;
