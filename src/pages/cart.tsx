import Head from 'next/head';
import React from 'react';
import Layout from '../components/layout';
import CartPage from '../containers/CartPage/CartPage';

const Cart = () => {
  return (
    <Layout active="cart">
      <Head>
        <title>Panier - Goodfood</title>
      </Head>
      <CartPage />
    </Layout>
  );
};

export default Cart;
