import Head from 'next/head';
import React from 'react';
import Layout from '../components/layout';
import LoginContainer from '../containers/Auth/Login/LoginContainer';

const login = () => {
  return (
    <Layout hasNavbar={false} hasHeader={false}>
      <Head>
        <title>Connexion - Goodfood</title>
      </Head>
      <LoginContainer />
    </Layout>
  );
};

export default login;
