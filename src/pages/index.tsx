import Head from 'next/head';
import React from 'react';
import Layout from '../components/layout';
import LandingPage from '../containers/LandingPage/LandingPage';

const Home = () => {
  return (
    <Layout hasNavbar={false} hasHeader={false}>
      <Head>
        <title>Goodfood</title>
      </Head>
      <LandingPage />
    </Layout>
  );
};

export default Home;
