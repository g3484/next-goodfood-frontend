import React, { useEffect } from 'react';
import '../../styles/globals.scss';
import type { AppProps } from 'next/app';
import '@fortawesome/fontawesome-svg-core/styles.css'; // import Font Awesome CSS
import { config } from '@fortawesome/fontawesome-svg-core';
config.autoAddCss = false;
import jwtDecode from 'jwt-decode';
import { getCurrentUser } from '../services/axios/authRequest';
import useCurrentUserStore from '../store/currentUserStore';

interface TokenDto {
  id: string;
  email: string;
  exp: number;
  iat: number;
}

const MyApp = ({ Component, pageProps }: AppProps) => {
  const { setCurrentUser } = useCurrentUserStore();

  const fetchCurrentUser = async () => {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) return;

    try {
      const user = jwtDecode(accessToken) as TokenDto;
      const userId = user.id;

      const {
        data,
        status,
      }: {
        data: any;
        status: number;
      } = await getCurrentUser(`/api/users/profile/${userId}`);

      if (status === 200) {
        const { user, address } = data;

        setCurrentUser(user, address);
      }
    } catch (err) {
      return;
    }
  };

  useEffect(() => {
    fetchCurrentUser();
  }, []);

  return <Component {...pageProps} />;
};

export default MyApp;
