import React from 'react';
import Head from 'next/head';
import Layout from '../components/layout';
import RegisterContainer from '../containers/Auth/Register/RegisterContainer';

const register = () => {
  return (
    <Layout hasNavbar={false} hasHeader={false}>
      <Head>
        <title>Inscription - Goodfood</title>
      </Head>
      <RegisterContainer />
    </Layout>
  );
};

export default register;
