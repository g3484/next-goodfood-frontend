import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import Layout from '../../components/layout';
import ProductPage from '../../containers/ProductPage/ProductPage';
import { getSingleProduct } from '../../services/axios/productRequest';

type Product = {
  id: string;
  name: string;
  imageUrl: string;
  price: number;
  description: string;
  idCategory: string;
};

interface ProductProps {
  productId: string;
}

const Product = ({ productId }: ProductProps) => {
  const [product, setProduct] = useState<Product>();

  const fetchProduct = async () => {
    const {
      data,
      status,
    }: {
      data: Product;
      status: number;
    } = await getSingleProduct(`/api/products/${productId}`);

    if (status === 200) {
      return setProduct(data);
    }

    if (status !== 200) {
      alert('error');
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  return (
    <Layout active="home" hasHeader={false}>
      <Head>
        <title>Goodfood</title>
      </Head>
      {product && <ProductPage product={product} />}
    </Layout>
  );
};

Product.getInitialProps = async ({ query }) => {
  return { productId: query.productId };
};

export default Product;
