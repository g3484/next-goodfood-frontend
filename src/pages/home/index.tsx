import Head from 'next/head';
import React from 'react';
import Layout from '../../components/layout';
import MainPage from '../../containers/MainPage/MainPage';

const Home = () => {
  return (
    <Layout active="home">
      <Head>
        <title>Goodfood</title>
      </Head>
      <MainPage />
    </Layout>
  );
};

export default Home;
